
from models.socio import Socio as SocioDB
from schemas.model_socio_pydantic import Socio
from fastapi.responses import JSONResponse
class SocioService():
    def __init__(self, db) -> None:
        self.db = db
    
    def get_socios(self):
        consulta_all_socios = self.db.query(SocioDB).all()
        return consulta_all_socios
    
    def get_socio_id(self, id:int):
        consulta_socio_id = self.db.query(SocioDB).filter(SocioDB.id == id).first()
        return consulta_socio_id
    
    def create_socio(self, socio:Socio):
        new_movie = SocioDB(**socio.dict())
        self.db.add(new_movie)
        self.db.commit()
        return
    
    def update_socio(self, id, socio:Socio):
        consulta_socio_id = self.db.query(SocioDB).filter(SocioDB.id == id).first()
        consulta_socio_id.nombres = socio.nombres
        consulta_socio_id.apellidos = socio.apellidos
        consulta_socio_id.dni = socio.dni
        consulta_socio_id.direccion_domiciliaria = socio.direccion_domiciliaria
        self.db.commit()
        return
    
    def delete_socio(self, id):
        consulta_id_socio = self.db.query(SocioDB).filter(SocioDB.id == id).first()
        self.db.delete(consulta_id_socio)
        self.db.commit()
        