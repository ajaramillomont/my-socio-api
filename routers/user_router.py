from fastapi import APIRouter
from utils.JWTmanager import crear_token
from fastapi.responses import JSONResponse
from schemas.model_user_pydantic import User

user_router = APIRouter()

@user_router.post('/socios/login', tags=['Login'])
def login(user: User):
    if user.username == 'puco1988' and user.password =='123456' and user.confirmar_password =='123456':
        token: str = crear_token(user.dict())
        return JSONResponse(status_code=200, content=token)
    else:
        return JSONResponse(status_code=500, content={'message':'No fue posible generar token para autenticación'})