from fastapi import APIRouter
from fastapi.responses import JSONResponse
from fastapi import Depends
from fastapi.encoders import jsonable_encoder
from models.socio import Socio as SocioDB
from config.database_conf import Session
from schemas.model_socio_pydantic import Socio
from middlewares.JWTBea import JWTBearer
from services.socio_service import SocioService
from services.socio_service import SocioService
socio_router = APIRouter()

@socio_router.get('/', tags=['Messages'])
def message(saludo:str):
    return JSONResponse(status_code=200, content=saludo)

@socio_router.get('/socios/', tags=['Socios'], dependencies=[Depends(JWTBearer())] )
def get_socios():
    db = Session()
    consulta_all_socios = SocioService(db).get_socios()
    return JSONResponse(status_code=200, content= jsonable_encoder(consulta_all_socios))

@socio_router.get('/socios/{id}', tags=['Socios'])
def get_socio_id(id: int):
    db = Session()
    consulta_socio_id = SocioService(db).get_socio_id(id)
    if not consulta_socio_id:
        return JSONResponse(status_code=404, content = {'message': 'El socio no existe'} )
    return JSONResponse(status_code=200, content = jsonable_encoder(consulta_socio_id))

@socio_router.post('/socios/', tags=['Socios'])
def create_socio(socio: Socio):
    db = Session()
    SocioService(db).create_socio(socio)
    return JSONResponse(status_code=201, content={'message':'El socio se registro con éxito'})

@socio_router.put('/socios/', tags=['Socios'])
def put_socio(id: int, socio:Socio):
    db = Session()
    consulta_id_socio = SocioService(db).get_socio_id(id)
    if not consulta_id_socio:
        return JSONResponse(status_code=404, content = {'message': 'El socio que intenta actualizar no existe'})
    SocioService(db).update_socio(id, socio)
    return JSONResponse(status_code=200, content = {'message': 'El socio se actualizo correctamente'} )
    

@socio_router.delete('/socios/', tags=['Socios'])
def delete_socio(id: int):
    db = Session()
    consulta_id_socio = SocioService(db).get_socio_id(id)
    if not consulta_id_socio:
            return JSONResponse(status_code=404, content = {'message': 'El socio que intenta eliminar no existe'} )
    SocioService(db).delete_socio(consulta_id_socio.id)
    return JSONResponse(status_code=200, content = {'message': 'El socio fue eliminado correctamente'} )