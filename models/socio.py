from config.database_conf import Base
from sqlalchemy import Column, Integer, String
class Socio(Base):
    
    __tablename__ = 'socios'
    
    id = Column(Integer, primary_key=True)
    nombres = Column(String)
    apellidos = Column(String)
    dni = Column(Integer)
    direccion_domiciliaria = Column(String)
    