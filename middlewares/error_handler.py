from starlette.middleware.base import BaseHTTPMiddleware
from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.responses import Response, JSONResponse
class ErrorHandler(BaseHTTPMiddleware):
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)
    
    async def dispatch(self, request: Request, call_next) -> Response | JSONResponse:
        try:
            return await call_next(request)
        except Exception as e:
            print(type(e))
            return JSONResponse(status_code=500, content={'error':str(e)})