from fastapi import HTTPException
from fastapi.requests import Request
from utils.JWTmanager import validar_token
from fastapi.security import HTTPBearer

class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data: dict = validar_token(auth.credentials)
        if data['username'] != "puco1988":
            raise HTTPException(status_code=500, detail='Autenticación fallida')