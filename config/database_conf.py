import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

### nombre de la base de datos
database_name = '../db_socios.sqlite'
### ruta de la base de datos
path_db =  os.path.dirname(os.path.realpath(__file__))
### url de la base de datos
url_db = f'sqlite:///{os.path.join(path_db, database_name)}'
###motor de la base de datos
engine = create_engine(url_db, echo=True)
#para la sesión con nuestro motor db
Session = sessionmaker(engine)
##La base de nuestros modelos ORM
Base = declarative_base()