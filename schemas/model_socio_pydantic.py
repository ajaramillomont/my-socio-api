from pydantic import BaseModel
from typing import Optional

class Socio(BaseModel):
    id: Optional[int] = None
    nombres: str
    apellidos: str
    dni: int
    direccion_domiciliaria: str
    
    class Config:
        schema_extra = {
            "example": {
                "nombres": "Andrés Armando",
                "apellidos": "Jaramillo Montaño",
                "dni": 1104733645,
                "direccion_domiciliaria": "barrio Motupe"
            }
        }