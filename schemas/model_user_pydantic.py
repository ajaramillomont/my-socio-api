from pydantic import BaseModel

class User(BaseModel):
    username: str
    password: str
    confirmar_password: str