from fastapi import FastAPI
from config.database_conf import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.socio_router import socio_router
from routers.user_router import user_router

app = FastAPI()

##Así podemos añadir middlewares a nuestra aplicación
app.add_middleware(ErrorHandler)

##importamos nuestras rutas en router_socio

app.include_router(socio_router)

app.include_router(user_router)

Base.metadata.create_all(bind=engine)


